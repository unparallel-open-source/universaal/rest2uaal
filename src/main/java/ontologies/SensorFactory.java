package ontologies;

import org.universAAL.middleware.rdf.Resource;
import org.universAAL.middleware.rdf.ResourceFactory;

public class SensorFactory implements ResourceFactory {

	@Override public Resource createInstance(String classURI, String instanceURI, int factoryIndex) {
		switch(factoryIndex) {
			case 0: return new HumiditySensor(instanceURI);
			default: return null;
		}
	}
}