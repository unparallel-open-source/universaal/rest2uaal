package ontologies;

import org.universAAL.ontology.device.DeviceOntology;
import org.universAAL.ontology.device.Sensor;

public class HumiditySensor extends Sensor {

	public static final String MY_URI = DeviceOntology.NAMESPACE + "HumiditySensor";
	public static final String PROP_HAS_VALUE = HumiditySensorOntology.NAMESPACE + "hasValue";
	public static final String PROP_HUMIDITY = HumiditySensorOntology.NAMESPACE + "humidity";
	
	public HumiditySensor() {
		super();
	}
	
	public HumiditySensor(String instanceURI) {
		super(instanceURI);
	}
	
	public String getClassURI() {
		return MY_URI;
	}

	public int getPropSerializationType(String propURI) {
		return PROP_HAS_VALUE.equals(propURI) ? PROP_SERIALIZATION_FULL : super.getPropSerializationType(propURI);
	}

	public boolean isWellFormed() {
		return true;
	}
}
