package mainpackage;

import java.io.IOException;
import java.text.ParseException;
import java.time.DateTimeException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.universAAL.context.che.Hub;
import org.universAAL.context.che.Hub.Log;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.container.osgi.OSGiContainer;
import org.universAAL.middleware.owl.OntologyManagement;

import middleware.Middleware;
import ontologies.HumiditySensorOntology;
import rest.RestClient;
import utils.Constants;


public class Activator implements BundleActivator {
	/* ---------------------------------- VARIABLES ----------------------------------- */
	private static Log log;
	private static ModuleContext moduleContext;
	private static RestClient client;
	private static Middleware middleware;
	private static ScheduledExecutorService scheduler;

	public void start(BundleContext bcontext) throws Exception {
		log = Hub.getLog(Activator.class);
		moduleContext = OSGiContainer.THE_CONTAINER.registerModule(new Object[] { bcontext });
		OntologyManagement.getInstance().register(moduleContext, new HumiditySensorOntology());
		client = new RestClient(-1);
		middleware = new Middleware(moduleContext);
		setDataListener();
	}

	public void stop(BundleContext arg0) throws Exception { 
		try {
			shutdownDataListener();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static void setDataListener() {
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override public void run() {
				try {
					String lastTimestamp = middleware.getLastTimestamp();
					String data = client.getData("?since=" + lastTimestamp);
					middleware.publishData(data);
					
					log.info("setDataListener", "Last timestamp: " + lastTimestamp);
					log.info("setDataListener", "\nData: " + data + "\n");
				} catch (IOException e) {
					log.error("setDataListener", "Connection refused. We will try again in " + Constants.REPEAT_TIME + " seconds", e);
				} catch(IllegalArgumentException e) {
					log.error("setDataListener", "Probably the ContextProvider is not well formed", e);
				} catch (ParseException e) {
					log.error("setDataListener", "Error on parsing data", e);
				} catch (DateTimeException e) {
					log.error("setDataListener", "Error on formatting last timestamp", e);
				}
			}
		}, Constants.START_TIME, Constants.REPEAT_TIME, TimeUnit.SECONDS);
	}
	
	private void shutdownDataListener() throws InterruptedException {
		if(!scheduler.isShutdown()) {
			scheduler.shutdown();
			
			if(!scheduler.awaitTermination(3, TimeUnit.SECONDS)) {
				scheduler.shutdownNow();
			}
		}
	}
}
