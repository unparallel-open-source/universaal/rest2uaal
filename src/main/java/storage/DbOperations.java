package storage;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.universAAL.context.che.Hub;
import org.universAAL.context.che.Hub.Log;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.context.ContextEvent;
import org.universAAL.middleware.owl.MergedRestriction;
import org.universAAL.middleware.rdf.PropertyPath;
import org.universAAL.middleware.service.CallStatus;
import org.universAAL.middleware.service.DefaultServiceCaller;
import org.universAAL.middleware.service.ServiceCaller;
import org.universAAL.middleware.service.ServiceRequest;
import org.universAAL.middleware.service.ServiceResponse;
import org.universAAL.middleware.service.owls.process.ProcessOutput;
import org.universAAL.ontology.che.ContextHistoryService;

import utils.Constants;

public class DbOperations {	
	/* ---------------------------------- VARIABLES ----------------------------------- */
	private static Log log;
	private ModuleContext moduleContext;
	
	/* --------------------------------- CONSTRUCTOR ---------------------------------- */
	public DbOperations(ModuleContext moduleContext) {
		log = Hub.getLog(DbOperations.class);
		this.moduleContext = moduleContext;
	}
	
	public String getLastTimestamp() throws DateTimeException {
		String query = prepareSPARQLQuery();
		ContextEvent event = getContextEvent(query, Constants.ABSOLUTE_TIMESTAMP_X73_ONTOLOGY);
		String data = (event != null) ? event.getRDFObject().toString() : "";
		
		return findLastTimestamp(data);
	}
	
	private String prepareSPARQLQuery() {
		String query = Constants.ONTOLOGY_PREFIX 
					 + " SELECT ?c WHERE { ?c" 
					 + " rdf:type <" + Constants.CONTEXT_EVENT_SUBJECT_TYPE_URI + "> ;" 
					 + " rdf:subject <" + Constants.LAST_SYNC_URI + "> ;" 
					 + " rdf:predicate <" + Constants.HAS_VALUE_PREDICATE_URI + "> . }";

		return query;
	}
	
	private ContextEvent getContextEvent(String query, String expectedOutput) {
		ServiceCaller caller = new DefaultServiceCaller(moduleContext);
		ServiceResponse response = caller.call(requestEventsBySPARQL(query, expectedOutput));

		if (response.getCallStatus() == CallStatus.succeeded) {
			List<?> value = getReturnValue(response.getOutputs(), expectedOutput);
			if (value != null) {
				ContextEvent[] events = (ContextEvent[])value.toArray(new ContextEvent[value.size()]);
				return events[events.length - 1];
			}
		}
		
		caller.close();
		
		return null;
	}
	
	private ServiceRequest requestEventsBySPARQL(String query, String expectedOutput) {
		MergedRestriction restriction = MergedRestriction.getFixedValueRestriction(Constants.PROPERTY_PROCESSES, query);
		String[] propertySources = new PropertyPath(null, false, Constants.PROPERTY_MANAGES).getThePath();
		ServiceRequest serviceRequest = new ServiceRequest(new ContextHistoryService(), null);		
		serviceRequest.getRequestedService().addInstanceLevelRestriction(restriction, new String[] { Constants.PROPERTY_PROCESSES });
		serviceRequest.addSimpleOutputBinding(new ProcessOutput(expectedOutput), propertySources);
		
		return serviceRequest;
	}

	private List<?> getReturnValue(List<?> outputs, String expectedOutput) {
		if (outputs != null) {
			for (Object entry : outputs) {
				ProcessOutput output2 = (ProcessOutput)entry;
				if (output2.getURI().equals(expectedOutput)) {
					return (List<?>)output2.getParameterValue();
				}
			}
		}

		return null;
	}
	
	private String findLastTimestamp(String data) throws DateTimeException {
		int temporaryTimestamp = 1;	// "1" corresponds to "1970-01-01T00:00:01Z", in case this variable does not change
		
		if(!data.equals("")) {
			data = data.substring(1, data.length() - 1);
			String[] dataArray = data.split(", ");
			
			for(String dataEntry : dataArray) {
				if(isInteger(dataEntry)) {
					temporaryTimestamp = (temporaryTimestamp > new Integer(dataEntry)) ? temporaryTimestamp : new Integer(dataEntry);
				}
			}
		}
		
		ZonedDateTime datetime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(++temporaryTimestamp), ZoneId.systemDefault());
		String timestamp = datetime.format(DateTimeFormatter.ofPattern(Constants.TIMESTAMP_PATTERN));
		
		return timestamp;
	}
	
	private boolean isInteger(String dataEntry) {
		try {
			new Integer(dataEntry);
		} catch (NumberFormatException e) {
			return false;
		}
		
		return true;
	}
}
