package rest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.universAAL.context.che.Hub;
import org.universAAL.context.che.Hub.Log;

import middleware.Middleware;
import utils.Constants;

public class RestClient {
	/* ---------------------------------- VARIABLES ----------------------------------- */
	private static Log log;
	private int port;

	/* --------------------------------- CONSTRUCTOR ---------------------------------- */
	public RestClient(int portToConnect) {
		log = Hub.getLog(Middleware.class);
		port = (portToConnect != -1) ? portToConnect : 4567;
	}

	public String getData(String queryParameter) throws IOException {
		HttpURLConnection httpConnection = getHttpConnection(queryParameter, Constants.GET_REQUEST);

		if(httpConnection.getResponseCode() == Constants.OK) {
			String response = "";
			Scanner httpScanner = new Scanner(httpConnection.getInputStream());

			while(httpScanner.hasNext()) { response += httpScanner.next(); }

			httpScanner.close();
			httpConnection.disconnect();
		
			log.info("getData", "\n\nResponse: " + response + "\n\n");
			return response;
		}

		return null;
	}
	
	private HttpURLConnection getHttpConnection(String urlQueryParameter, String requestMethod) throws IOException {
		URL url = new URL(String.format(Constants.DATA_URL, port) + urlQueryParameter);
		HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
		httpConnection.setRequestMethod(requestMethod);

		return httpConnection;
	}
}
