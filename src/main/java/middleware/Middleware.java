package middleware;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.universAAL.context.che.Hub;
import org.universAAL.context.che.Hub.Log;
import org.universAAL.middleware.container.ModuleContext;
import org.universAAL.middleware.context.ContextEvent;
import org.universAAL.middleware.context.ContextPublisher;
import org.universAAL.middleware.context.owl.ContextProvider;
import org.universAAL.middleware.context.owl.ContextProviderType;
import org.universAAL.middleware.rdf.Resource;
import org.universAAL.ontology.X73.AbsoluteTimeStamp;
import org.universAAL.ontology.device.TemperatureSensor;
import org.universAAL.ontology.device.ValueDevice;
import org.universAAL.ontology.healthmeasurement.owl.HeartRate;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ontologies.HumiditySensor;
import storage.DbOperations;
import utils.Constants;


public class Middleware {
	/* ---------------------------------- VARIABLES ----------------------------------- */
	private static Log log;
	private DbOperations db;
	private ModuleContext moduleContext;
	
	/* --------------------------------- CONSTRUCTOR ---------------------------------- */
	public Middleware(ModuleContext moduleContext) {
		log = Hub.getLog(Middleware.class);
		db = new DbOperations(moduleContext);
		this.moduleContext = moduleContext;
	}

	/*public void publishData(String data) throws IllegalArgumentException, ParseException {
		List<String> parsedData = parseData(data);

		if(parsedData != null) {
			for(String value : parsedData) {
				publishHeartRateData(value.substring(value.indexOf(",") + 1));
				publishLastSyncTimestamp(value.substring(0, value.indexOf(",")));
			}
		}
	}*/
	
	public void publishData(String data) throws IllegalArgumentException, ParseException {
		List<JsonObject> parsedData = parseData(data);
		String value, dataType;
		
		if(parsedData != null) {
			for(JsonObject jsonObject : parsedData) {
				dataType = toString(jsonObject.get("dataType"));
				value = toString(jsonObject.get("parsedData"));
				
				switch(dataType) {
					case "HeartRate": {
						publishHeartRateData(value);
						log.info("publishData", "Publishing Heart Rate Event: " + value);
						break;
					}
					case "Temperature": {
						publishTemperatureData(value);
						log.info("publishData", "Publishing Temperature Event: " + value);
						break;
					}
					case "Humidity": {
						publishHumidityData(value);
						log.info("publishData", "Publishing Humidity Event: " + value);
						break;
					}
					default: break;
				}
				
				/*if(toString(jsonObject.get("dataType")).equals("Heart Rate")) {
					log.info("publishData", "Publishing Heart Rate Event...");
					publishHeartRateData(toString(jsonObject.get("parsedData")));
				} else if(toString(jsonObject.get("dataType")).equals("Temperature")) {
					log.info("publishData", "Publishing Temperature Event...");
					publishTemperatureData(toString(jsonObject.get("parsedData")));
				}*/
				//publishHeartRateData(value.substring(value.indexOf(",") + 1));
				//publishLastSyncTimestamp(value.substring(0, value.indexOf(",")));
				publishLastSyncTimestamp(toString(jsonObject.get("timestamp")));
			}
		}
	}
	
	public String getLastTimestamp() {
		return db.getLastTimestamp();
	}
	
	private List<JsonObject> parseData(String data) {
		JsonElement jsonTree = new JsonParser().parse(data);
		List<JsonObject> dataList = new ArrayList<JsonObject>();
		
		if(jsonTree.isJsonArray()) {
			JsonObject jsonObject;
			//String timestamp, parsedData;
			
			for(JsonElement jsonElement : jsonTree.getAsJsonArray()) {
				if(jsonElement.isJsonObject()) {
					jsonObject = jsonElement.getAsJsonObject();
					/*timestamp = toString(jsonObject.get("timestamp"));
					parsedData = toString(jsonObject.get("parsedData"));*/
					dataList.add(jsonElement.getAsJsonObject());
					
					log.info("parseData", "Timestamp: " + toString(jsonObject.get("timestamp")) + ", DeviceID: " 
							 + toString(jsonObject.get("deviceId")) + ", Data: " + toString(jsonObject.get("parsedData")));
				}
			}
		}
		
		return dataList;
	}
	
	/*private List<String> parseData(String data) {
		JsonElement jsonTree = new JsonParser().parse(data);
		List<String> dataList = new ArrayList<String>();
		
		if(jsonTree.isJsonArray()) {
			JsonObject jsonObject;
			String timestamp, parsedData;
			
			for(JsonElement jsonElement : jsonTree.getAsJsonArray()) {
				if(jsonElement.isJsonObject()) {
					jsonObject = jsonElement.getAsJsonObject();
					timestamp = toString(jsonObject.get("timestamp"));
					parsedData = toString(jsonObject.get("parsedData"));
					dataList.add(timestamp + "," + parsedData);
					
					log.info("parseData", "Timestamp: " + timestamp + ", DeviceID: " + toString(jsonObject.get("deviceId")) + ", Data: " + parsedData);
				}
			}
		}
		
		return dataList;
	}*/
	
	private void publishEvent(int data, Resource subject) {
		ContextProvider contextProvider = getContextProvider();
		ContextEvent event = generateContextEvent(data, subject, contextProvider);
		ContextPublisher publisher = getContextPublisher(contextProvider);
		
		publisher.publish(event);
		publisher.close();
	}
	
	private void publishHeartRateData(String data) throws ParseException {
		int value = Integer.parseInt(data);
		publishEvent(value, new HeartRate(Constants.HEART_RATE_URI));
	}
	
	private void publishHumidityData(String data) throws ParseException {
		int value = Integer.parseInt(data);
		publishEvent(value, new HumiditySensor("urn:org.universAAL.rest:Humidity"));
	}
	
	private void publishTemperatureData(String temperature) {
		ContextProvider contextProvider = getContextProvider();
		ContextEvent event = new ContextEvent(Constants.CONTEXT_EVENT_URI);
		event.setRDFSubject(new TemperatureSensor("urn:org.universAAL.rest:Temperature"));
		event.setRDFPredicate(ValueDevice.PROP_HAS_VALUE);
		event.setRDFObject(temperature);
		event.setResourceLabel("Label");
		ContextPublisher publisher = getContextPublisher(contextProvider);
		
		publisher.publish(event);
		publisher.close();
		
		//publishEvent(temperature, new TemperatureSensor("urn:org.universAAL.rest:Temperature"));
	}
	
	private void publishLastSyncTimestamp(String data) throws ParseException {
		int timestamp = (int)(new SimpleDateFormat(Constants.TIMESTAMP_PATTERN).parse(data).getTime()/1000);
		publishEvent(timestamp, new AbsoluteTimeStamp(Constants.LAST_SYNC_URI));
	}
	
	private ContextProvider getContextProvider() {
		ContextProvider contextProvider = new ContextProvider(Constants.CONTEXT_URI);
		contextProvider.setType(ContextProviderType.gauge);
		
		return contextProvider;
	}
	
	private ContextPublisher getContextPublisher(ContextProvider contextProvider) throws IllegalArgumentException {
		ContextPublisher publisher = new ContextPublisher(moduleContext, contextProvider) {
			@Override public void communicationChannelBroken() {
				close();
				log.warn("getContextPublisher", "Publisher's Communication Channel is now broken!");
			}
		};
		
		return publisher;
	}
	
	private ContextEvent generateContextEvent(int data, Resource subject, ContextProvider contextProvider) {
		ContextEvent event = new ContextEvent(Constants.CONTEXT_EVENT_URI);
		event.setRDFSubject(subject);
		event.setRDFPredicate(ValueDevice.PROP_HAS_VALUE);
		event.setRDFObject(data);
		event.setResourceLabel("Label");
		
		return event;
	}
	
	// the jsonElement comes with the attribute between "", so this method's
	// aim is to delete those ""
	private String toString(JsonElement jsonElement) {
		String jsonString = jsonElement.toString();
		
		return jsonString.substring(1, jsonString.length() - 1);
	}
}
